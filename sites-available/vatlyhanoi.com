
server {


    server_name vatlyhanoi.com   www.vatlyhanoi.com;


    access_log /var/log/nginx/vatlyhanoi.com.access.log rt_cache; 
    error_log /var/log/nginx/vatlyhanoi.com.error.log;


    root /var/www/vatlyhanoi.com/htdocs;
    
    

    index index.php index.html index.htm;


    include common/w3tc-hhvm.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/vatlyhanoi.com/conf/nginx/*.conf;
}
