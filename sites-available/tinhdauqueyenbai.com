server {


    server_name tinhdauqueyenbai.com   www.tinhdauqueyenbai.com;


    access_log /var/log/nginx/tinhdauqueyenbai.com.access.log rt_cache;
    error_log /var/log/nginx/tinhdauqueyenbai.com.error.log;


    root /var/www/tinhdauqueyenbai.com/htdocs;



    index index.php index.html index.htm;


    include common/w3tc-hhvm.conf;
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/tinhdauqueyenbai.com/conf/nginx/*.conf;
}

