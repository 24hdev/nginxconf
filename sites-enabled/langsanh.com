server {


    server_name langsanh.com   www.langsanh.com;


    access_log /var/log/nginx/langsanh.com.access.log rt_cache; 
    error_log /var/log/nginx/langsanh.com.error.log;


    root /var/www/langsanh.com/htdocs;
    
    

    index index.php index.html index.htm;


    include common/w3tc-hhvm.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/langsanh.com/conf/nginx/*.conf;
}
