server {


    server_name blog.24hdev.com   www.blog.24hdev.com;


    access_log /var/log/nginx/blog.24hdev.com.access.log rt_cache;
    error_log /var/log/nginx/blog.24hdev.com.error.log;


    root /var/www/blog.24hdev.com/htdocs;



    index index.php index.html index.htm;


    include common/w3tc-hhvm.conf;
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/blog.24hdev.com/conf/nginx/*.conf;
}

