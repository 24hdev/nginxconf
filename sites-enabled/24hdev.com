server {


    server_name 24hdev.com   www.24hdev.com;


    access_log /var/log/nginx/24hdev.com.access.log rt_cache; 
    error_log /var/log/nginx/24hdev.com.error.log;


    root /var/www/24hdev.com/htdocs;
    
    

    index index.php index.html index.htm;


    include common/w3tc-hhvm.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/24hdev.com/conf/nginx/*.conf;
}