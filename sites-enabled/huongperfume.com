server {


    server_name huongperfume.com  www.huongperfume.com;


    access_log /var/log/nginx/huongperfume.com.access.log rt_cache;
    error_log /var/log/nginx/huongperfume.com.error.log;


    root /var/www/huongperfume.com/htdocs;



    index index.php index.html index.htm;


    include common/w3tc-hhvm.conf;
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/huongperfume.com/conf/nginx/*.conf;
}

